import mobile1 from '../images/mobile-1.jpg';
import mobile2 from '../images/mobile-2.jpg';
import mobile3 from '../images/mobile-3.jpg';
import mobile4 from '../images/mobile-4.jpg';
import mobile5 from '../images/mobile-5.jpg';
import mobile6 from '../images/mobile-6.jpg';
import iphone from '../images/iphone.png';
import city1 from '../images/city-1.jpg'
import city2 from '../images/city-2.jpg'


// assume that this is the api data
const initState = {
  cardsData: [
    {
      id: 0,
      title: 'Landing Pages',
      body: 'Impress with these beautiful landing pages.',
      img: mobile1
    },
    {
      id: 1,
      title: 'Authentication',
      body: 'Awesome collection of pages for any scenario.',
      img: mobile2
    },
    {
      id: 2,
      title: 'Shop Pages',
      body: 'Complete front-end flow for e-commerce website.',
      img: mobile3
    },
    {
      id: 3,
      title: 'Secondary Pages',
      body: 'Awesome collection of pages for any scenario.',
      img: mobile4
    },
    {
      id: 4,
      title: 'Utility Pages',
      body: 'Error pages and everything else can be found here.',
      img: mobile5
    },
    {
      id: 5,
      title: 'Account Pages',
      body: 'Profile and account managemend made cool.',
      img: mobile6
    }
  ],

  projectsData: [
    {
      id: 0,
      title: 'Created with the latest technologies',
      body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea possimus nesciunt autem vero voluptatum deleniti.',
      className: 'bg-gradient-primary shadow-primary',
      icon: 'fab fa-html5'
    },
    {
      id: 1,
      title: 'Created with the latest technologies',
      body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea possimus nesciunt autem vero voluptatum deleniti.',
      className: 'bg-gradient-warning shadow-warning',
      icon: 'fab fa-node-js'
    },
    {
      id: 2,
      title: 'Created with the latest technologies',
      body: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea possimus nesciunt autem vero voluptatum deleniti.',
      className: 'bg-gradient-info shadow-info',
      icon: 'far fa-thumbs-up'
    }
  ],

  featuresData: {
    feature1: [
      {
        id: 0,
        title: 'Responsive web design',
        body: 'Responsive web design Modular and interchangable componente between layouts and even demos.',
        className: 'bg-gradient-primary shadow-primary',
        icon: 'fab fa-html5'
      },
      {
        id: 1,
        title: 'Responsive web designLoaded with features',
        body: 'Responsive web design Modular and interchangable componente between layouts and even demos.',
        className: 'bg-gradient-primary shadow-primary',
        icon: 'fab fa-html5'
      },
      {
        id: 2,
        title: 'Friendly online support',
        body: 'Responsive web design Modular and interchangable componente between layouts and even demos.',
        className: 'bg-gradient-primary shadow-primary',
        icon: 'fab fa-html5'
      }
    ],
    image: {
      id: 'img',
      img: iphone
    },
    feature2: [
      {
        id: 4,
        title: 'Free updates forever',
        body: 'Responsive web design Modular and interchangable componente between layouts and even demos.',
        className: 'bg-gradient-primary shadow-primary',
        icon: 'fab fa-html5'
      },
      {
        id: 5,
        title: 'Built with Sass',
        body: 'Responsive web design Modular and interchangable componente between layouts and even demos.',
        className: 'bg-gradient-primary shadow-primary',
        icon: 'fab fa-html5'
      },
      {
        id: 6,
        title: 'Infinite colors',
        body: 'Responsive web design Modular and interchangable componente between layouts and even demos.',
        className: 'bg-gradient-primary shadow-primary',
        icon: 'fab fa-html5'
      }
    ]
  },
  contactData: [
    {
      id: 0,
      title: 'Davao City',
      email: 'davao@company.com',
      tel: '0755.222.333',
      class: 'bg-gradient-dark',
      bg: {
        backgroundImage: `url('${city1}')`
      },
      height: {
        minHeight: '250px'
      }
    },
    {
      id: 1,
      title: 'Manila',
      email: 'davao@company.com',
      tel: '0755.222.333',
      class: 'bg-gradient-primary',
      bg: {
        backgroundImage: `url('${city2}')`
      },
      height: {
        minHeight: '250px'
      }
    }
  ]
}

export default initState;