import React from 'react';
import Login from './Login';
import Register from './Register';
import Layout from '../../layouts/layout';

export function SignIn() {
  return (
    <Layout>
      <section className="slice section-rotate slice-lg min-vh-100 d-flex align-items-center bg-section-secondary">
        <div className="section-inner bg-gradient-primary"></div>
        <div className="bg-absolute-cover bg-size--contain d-flex align-items-center"></div>
        <div className="container py-5 px-md-0 d-flex align-items-center">
          <div className="w-100">
            <div className="row row-grid justify-content-center justify-content-lg-between align-items-center">
              <Login />
              { sideSection }
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export function SignUp() {
  return (
    <Layout>
      <section className="slice section-rotate slice-lg min-vh-100 d-flex align-items-center bg-section-secondary">
        <div className="section-inner bg-gradient-primary"></div>
        <div className="bg-absolute-cover bg-size--contain d-flex align-items-center"></div>
        <div className="container py-5 px-md-0 d-flex align-items-center">
          <div className="w-100">
            <div className="row row-grid justify-content-center justify-content-lg-between align-items-center">
              <Register />
              { sideSection }
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

const sideSection = (
  <div className="col-lg-5 order-lg-1 d-none d-lg-block">
    <blockquote>
      <h3 className="h2 mb-4 text-white">Keep your face always toward the sunshine - and shadows will fall behind you.</h3>
      <footer>— <cite className="text-lg text-white">John Sulivan</cite></footer>
    </blockquote>
  </div>
)
