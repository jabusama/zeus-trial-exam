import React from 'react';
import Layout from '../../layouts/layout';
import MainHeader from '../../components/sections/MainHeader'
import Cards from '../../components/sections/Cards'
import HighLights from '../../components/sections/HightLights'
import Projects from '../../components/sections/Projects'
import Feature from '../../components/sections/Feature'
import AboutUs from '../../components/sections/AboutUs'

export default function index() {

  return (
    <Layout>
      {/* main header */}
      <MainHeader />
      {/* Cards */}
      <Cards />
      {/* highLights */}
      <HighLights />
      {/* projects */}
      <Projects />
      {/* features */}
      <Feature />
      {/* About us */}
      <AboutUs />
      {/* Contact Us */}
    </Layout>
  )
}
