import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import ContactUs from './sections/ContactUs'
import logo from '../images/imac-1.svg'

export default function Footer() {

  const imgStyle = {
    height: "70px" 
  }

  return (
    <Fragment >

      <section className="slice slice-lg" id="contact-us">
        <div className="container">
          <ContactUs />
        </div>
      </section>
      
      <footer id="footer-main">
        <div className="footer footer-dark bg-gradient-primary footer-rotate">
          <div className="container">
            <div className="row pt-md">
              <div className="col-lg-4 mb-5 mb-lg-0">
                <a href="#home">
                  <img src={logo} alt="Footer logo" style={imgStyle} />
                </a>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ea beatae amet et distinctio totam adipisci libero odit asperiores illum pariatur?</p>
              </div>
              <div className="col-lg-2 col-6 col-sm-4 ml-lg-auto mb-5 mb-lg-0">
                <h6 className="heading mb-3">Account</h6>
                <ul className="list-unstyled">
                  <li><Link to="pages/account-profile.html">Profile</Link></li>
                  <li><Link to="pages/account-settings.html">Settings</Link></li>
                  <li><Link to="pages/account-billing.html">Billing</Link></li>
                  <li><Link to="pages/account-notifications.html">Notifications</Link></li>
                </ul>
              </div>
              <div className="col-lg-2 col-6 col-sm-4 mb-5 mb-lg-0">
                <h6 className="heading mb-3">About</h6>
                <ul className="list-unstyled text-small">
                  <li><Link to="#">Services</Link></li>
                  <li><Link to="#">Contact</Link></li>
                  <li><Link to="#">Careers</Link></li>
                </ul>
              </div>
              <div className="col-lg-2 col-sm-4 mb-5 mb-lg-0">
                <h6 className="heading mb-3">Company</h6>
                <ul className="list-unstyled">
                  <li><Link to="#">Terms</Link></li>
                  <li><Link to="#">Privacy</Link></li>
                  <li><Link to="#">Support</Link></li>
                </ul>
              </div>
            </div>

            <div className="row align-items-center justify-content-md-between py-4 mt-4 delimiter-top">
              <div className="col-md-6">
                <div className="copyright text-sm font-weight-bold text-center text-md-left">
                  © 2020 <Link to="#" className="font-weight-bold" target="_blank">make changes</Link>. All rights reserved.
                </div>
              </div>
              <div className="col-md-6">
                <ul className="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                  <li className="nav-item">
                    <Link className="nav-link" to="https://dribbble.com" target="_blank">
                      <i className="fab fa-dribbble"></i>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="https://www.instagram.com" target="_blank">
                      <i className="fab fa-instagram"></i>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="https://github.com" target="_blank">
                      <i className="fab fa-github"></i>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="https://www.facebook.com" target="_blank">
                      <i className="fab fa-facebook"></i>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>

          </div>  
        </div>
      </footer>
    </Fragment>
  )
}
