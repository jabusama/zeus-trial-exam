import React, { useState } from 'react';
import { NavLink, Link } from 'react-router-dom';
import routeLinks from '../routes';
import logo from '../images/imac-1.svg'

export default function Header() {

  const [state, setState] = useState(false);

  const navToggle = () => {
    setState(!state);
  }

  const logoStyles = {
    height: '50px'
  }

  return (
    <div>
      <header className={`header header-transparent ${state ? 'header-collapse-show' : ''}`} id="header-main">
        <nav 
          className={`navbar navbar-main navbar-expand-lg navbar-transparent navbar-dark bg-dark ${state ? 'navbar-collapsed' : ''}`}
          id="navbar-main"
        >
          <div className="container px-lg-0">
            <Link to='/' className="navbar-brand mr-lg-5">
              <img src={logo} alt="logo" id="navbar-logo" style={logoStyles}/>
            </Link>
            <button 
              className="navbar-toggler pr-0" 
              type="button" 
              onClick={navToggle}
              data-toggle="collapse" 
              data-target="#navbar-main-collapse" 
              aria-controls="navbar-main-collapse" 
              aria-expanded="false" 
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="navbar-collapse collapse" id="navbar-main-collapse">
              <ul className="navbar-nav align-items-lg-center">
                {
                  routeLinks.map(link => (
                    link.key === "register" || link.key === "login" ? 
                        null
                      :
                      window.location.pathname === '/' ? 
                        link.key === 'home' ? 
                          <li className="nav-item" key={link.key}>
                            <NavLink className="nav-link" exact to={link.path}>{link.label}</NavLink>
                          </li>
                          :
                          <li className="nav-item" key={link.key}>
                            <a className="nav-link" href={link.path}>{link.label}</a>
                          </li>
                        :
                        link.key === 'home' ? 
                          <li className="nav-item" key={link.key}>
                            <NavLink className="nav-link" exact to={link.path}>{link.label}</NavLink>
                          </li>
                          :
                          null
                  ))
                }
              </ul>

              <ul className="navbar-nav align-items-lg-center ml-lg-auto">
                <li className="nav-item" >
                  <NavLink className="nav-link" to='/login'>Login</NavLink>
                </li>
                <li className="nav-item mr-0">
                  <NavLink to='/register' className="nav-link d-lg-none">Register</NavLink>
                  <NavLink 
                    className="btn btn-sm btn-white rounded-pill btn-icon rounded-pill d-none d-lg-inline-flex" 
                    to='/register'
                  >
                    <span 
                      className="btn-inner--icon"
                    >
                      <i className="fas fa-shopping-cart"></i>
                    </span>
                    <span className="btn-inner--text">Register</span>
                  </NavLink>
                </li>
              </ul>

            </div>
          </div>
        </nav>
      </header>
    </div>
  )
}
