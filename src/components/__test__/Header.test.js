import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import Header from '../Header';

afterEach(cleanup);

test('renders without crashing', () => {
  const div = document.createElement("div");
  render(<Header></Header>, div);
})
