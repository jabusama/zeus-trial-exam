import React from 'react'
import { Link } from 'react-router-dom'

export default function MainHeader() {

  return (
    <section className="header-1 section-rotate bg-section-secondary" data-offset-top="#header-main" id="home">
      <div className="section-inner bg-gradient-primary"></div>
      <div className="bg-absolute-cover bg-size--contain d-flex align-items-center"></div>
      <div className="container position-relative zindex-100">
        <div className="row row-grid justify-content-around align-items-center">
          <div className="col-lg-5">
            <div className="text-center text-lg-left">
              <span className="badge badge-soft-primary badge-pill">Lorem, ipsum.</span>
              <h1 className="text-white mt-4 mb-3">Lorem, ipsum dolor.</h1>
              <p className="lead lh-180 text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis dolores repellat odit ipsa nostrum omnis distinctio impedit veniam, quod ex.</p>
              <div className="row text-center">
                <div className="col mx-0">
                  <div className="mt-6">
                    <Link to="/login" className="btn btn-white btn-icon rounded-pill hover-translate-y-n3">
                      <span className="btn-inner--icon"><i className="fas fa-fire"></i></span>
                      <span className="btn-inner--text">Login</span>
                    </Link>
                  </div>
                </div>
                <div className="col mx-0">
                <div className="mt-6">
                  <Link to="/register" className=" btn btn-dark btn-icon rounded-pill hover-translate-y-n3">
                    <span className="btn-inner--icon"><i className="fas fa-fire"></i></span>
                    <span className="btn-inner--text">Register</span>
                  </Link>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div className="col-lg-5 d-none d-lg-block">
            <div className="card bg-section-secondary mt-4 mb-0 py-5 px-4 shadow-lg perspective-right hover-scale-110">
              <div className="card-body">
                <h2 className="heading h2">
                  <strong>Design</strong> is thinking made visual.
                </h2>
                <p className="lead lh-180 mt-4">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna. Ut enim ad minim veniam quis nostrud exercitation.</p>
                <ul className="list-unstyled mt-4">
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <div className="icon icon-shape icon-primary icon-sm rounded-circle mr-3">
                          <i className="far fa-store-alt"></i>
                        </div>
                      </div>
                      <div>
                        <span className="h6 mb-0">Perfect for modern startups</span>
                      </div>
                    </div>
                  </li>
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <div className="icon icon-shape icon-warning icon-sm rounded-circle mr-3">
                          <i className="far fa-palette"></i>
                        </div>
                      </div>
                      <div>
                        <span className="h6 mb-0">Built with ease-of-use at its core</span>
                      </div>
                    </div>
                  </li>
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <div className="icon icon-shape icon-success icon-sm rounded-circle mr-3">
                          <i className="far fa-cog"></i>
                        </div>
                      </div>
                      <div>
                        <span className="h6 mb-0">Quality design that is meant to last</span>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
