import React from 'react'
import useGlobal from '../../services/useGLobal';





export default function Cards() {

  const [gStates, gActions] = useGlobal();

  const imgStyle = {
    height: '70px'
  }

  return (
    <section id="sct-page-examples" className="slice bg-section-secondary">
      <div className="container">
        <div className="row">
          
          {gStates.cardsData.map(data => (
            <div className="col-lg-4 col-sm-6" key={data.id}>
              <div className="card text-center hover-shadow-lg hover-translate-y-n10">
                <div className="px-4 py-5">
                  <img alt=" placeholder" src={data.img} className="svg-inject" style={imgStyle} />
                </div>
                <div className="px-4 pb-5">
                  <h5>{data.title}</h5>
                  <p className="text-muted">{data.body}</p>
                </div>
              </div>
            </div>
          ))}
          
        </div>
      </div>
      <div className="fluid-paragraph text-center mt-5">
        <p>
          <strong className="text-primary">Lorem, ipsum dolor!</strong> Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sequi corrupti, repellat modi ut dolore ea voluptatum maiores debitis quae..
        </p>
      </div>
    </section>
  )
}
