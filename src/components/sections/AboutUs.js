import React from 'react';
import { Link } from 'react-router-dom';

import aboutUsBg from '../../images/work-harder.jpg'


export default function AboutUs() {
  
  const aboutUsStyles = {
    background: {
      backgroundImage: `url('${aboutUsBg}')`
    },
    height: {
      height: '597px'
    }
  }
  
  return (
    <section 
        className="spotlight bg-cover bg-size--cover" 
        data-spotlight="fullscreen" 
        style={aboutUsStyles.background}
        id="about-us"
      >
        <span className="mask bg-dark opacity-7"></span>
        <div className="spotlight-holder pt-9 pb-6 py-lg-0" style={aboutUsStyles.height}>
          <div className="container d-flex align-items-center px-0">
            <div className="col">
              <div className="row row-grid">
                <div className="col-lg-6">
                  <div className="py-6 text-center text-lg-left">
                    <h2 className="display-4 text-white mb-4">About us</h2>
                    <p className="lead lh-180 text-white">With we want to optimize the customization process so your team can save time when building websites.</p>
                    <div className="mt-5">
                      <a 
                        href="#contact-us" 
                        className="btn btn-white rounded-pill px-5 mr-lg-4 box-shadow-2"
                      >
                        Contact us
                      </a>
                      <Link 
                        to="https://www.youtube.com" 
                        className="btn btn-outline-white btn-icon rounded-pill px-4" 
                        data-fancybox=""
                      >
                        <span className="btn-inner--icon"><i className="far fa-play"></i></span>
                        <span className="btn-inner--text">Watch our video</span>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
  )
}
