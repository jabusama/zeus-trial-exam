import React, { Fragment } from 'react';
import useGlobal from '../../services/useGLobal';

export default function ContactSection() {

  const [gStates, gActions] = useGlobal(); 

  return (
    <Fragment>
      <div className="row justify-content-center">
        <div className="col-lg-8 text-center mb-5">
          <h3 className="display-4 mb-4">Contact us</h3>
          <p className="lead">If there's something we can help you with, jut let us know. We'll be more than happy to offer you our help</p>
        </div>
        {
          gStates.contactData.map(contact => (
            <div className="col-md-6" key={contact.id}>
              <div className="card  overflow-hidden shadow hover-shadow-lg border-0 position-relative zindex-100">
                <div className="card-img-bg" style={contact.bg}></div>
                <span className={`mask opacity-9 opacity-8--hover ${contact.class}`}></span>
                <div className="card-body px-5 py-5">
                  <div style={contact.height}>
                    <h2 className="h2 text-white font-weight-bold mb-4">{contact.title}</h2>
                    <h5 className="text-white mt-4 mb-2">email: {contact.email}</h5>
                    <h5 className="text-white">Tel. No.: {contact.tel}</h5>
                  </div>
                  <span className="text-white text-uppercase font-weight-bold">
                    See on map
                    <i className="fas fa-angle-right ml-2"></i>
                  </span>
                </div>
              </div>
            </div>
          ))
        }
        
      </div>
    </Fragment>
  )
}
