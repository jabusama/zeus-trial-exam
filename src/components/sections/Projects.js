import React from 'react'
import useGlobal from '../../services/useGLobal'

export default function Projects() {

  const [gStates, gActions] = useGlobal();

  return (
    <section className="slice slice-lg bg-section-secondary overflow-hidden" id="projects">
      <div className="container position-relative zindex-100">
          <div className="mb-5 px-3 text-center">
              <span className="badge badge-soft-success badge-pill badge-lg">
                  Build tools
              </span>
              <h3 className=" mt-4">Our Projects</h3>
              <div className="fluid-paragraph mt-3">
                  <p className="lead lh-180">Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe labore perspiciatis dicta quidem. Quo tenetur aspernatur, ea rem quam eius.</p>
              </div>
          </div>
          <div className="row">

            {gStates.projectsData.map(project => (
              <div className="col-lg-4" key={project.id}>
                <div className="card px-3">
                  <div className="card-body py-5">
                      <div className="d-flex align-items-center">
                          <div className={`icon text-white rounded-circle icon-shape ${project.className}`}>
                              <i className={project.icon}></i>
                          </div>
                          <div className="icon-text pl-4">
                              <h5 className="mb-0">{project.title}</h5>
                          </div>
                      </div>
                      <p className="mt-4 mb-0">{project.body}</p>
                  </div>
                </div>
              </div>
            ))}
          
          </div>
      </div>
    </section>
  )
}
