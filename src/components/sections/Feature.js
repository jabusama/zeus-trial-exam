import React from 'react';
import useGlobal from '../../services/useGLobal';
export default function Feature() {

  const [gStates, gActions] = useGlobal();

  const supportImgStyle = {
    zIndex: '10'
  }

  return (
    <section className="slice slice-lg bg-gradient-primary">
      <div className="container">
        <div className="mb-5 text-center">
          <h3 className="text-white mt-4">Powerful features</h3>
          <div className="fluid-paragraph mt-3">
            <p className="lead lh-180 text-white">Start building fast, beautiful and modern looking websites in no time using our theme.</p>
          </div>
        </div>

        <div className="row row-grid align-items-center">
          {Object.keys(gStates.featuresData).map((support, i) => (
                
            <div className="col-lg-4" key={i}>
              {
                support === 'image' ? 
                  <div className="position-relative" style={supportImgStyle}>
                    <img alt="placeholder" src={gStates.featuresData[support].img} className="img-center img-fluid" />
                  </div>
                :
                gStates.featuresData[support].map(group => (
                  <div className="d-flex align-items-start mb-5" key={group.id}>
                    <div className="pr-4">
                      <div className="icon icon-shape bg-white text-primary box-shadow-3 rounded-circle">
                        <i className={ group.icon }></i>
                      </div>
                    </div>
                    <div className="icon-text">
                      <h5 className="h5 text-white">{ group.title }</h5>
                      <p className="mb-0 text-white">{ group.body }</p>
                    </div>
                  </div>
                ))
              }
            </div> 
              
          ))}
        
        </div>
      </div>
    </section>
  )
}
