import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import marketingImage from '../../images/marketing-1.svg';
import smartPhones from '../../images/smartphones.png'

export default function HightLights() {

  return (
    <Fragment >
      <section className="slice slice-lg">
        <div className="container">
          <div className="row row-grid justify-content-around align-items-center">
            <div className="col-lg-5 order-lg-2">
              <div className=" pr-lg-4">
                <h5 className=" h3">Change the way you build wesites. Forever.</h5>
                <p className="lead mt-4 mb-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam sapiente sunt dicta molestiae et magnam?</p>
                <Link to="/register" className="link link-underline-primary">Register now</Link>
              </div>
            </div>
            <div className="col-lg-6 order-lg-1 hover-scale-110">
              <img alt="placeholder" src={ marketingImage } className="img-fluid img-center" />
            </div>
          </div>
        </div>
      </section>

      <section className="slice slice-lg">
        <div className="container">
          <div className="row row-grid justify-content-around align-items-center">
            <div className="col-lg-5">
              <div className="">
                <h5 className=" h3">Lorem ipsum dolor sit amet consectetur. &amp; projects</h5>
                <p className="lead my-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt sint sed autem reiciendis, eum libero.</p>
                <ul className="list-unstyled">
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <div className="icon icon-shape icon-primary icon-sm rounded-circle mr-3">
                          <i className="fas fa-store-alt"></i>
                        </div>
                      </div>
                      <div>
                        <span className="h6 mb-0">Lorem ipsum dolor sit</span>
                      </div>
                    </div>
                  </li>
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <div className="icon icon-shape icon-warning icon-sm rounded-circle mr-3">
                          <i className="fas fa-palette"></i>
                        </div>
                      </div>
                      <div>
                        <span className="h6 mb-0">Lorem ipsum dolor sit amet</span>
                      </div>
                    </div>
                  </li>
                  <li className="py-2">
                    <div className="d-flex align-items-center">
                      <div>
                        <div className="icon icon-shape icon-success icon-sm rounded-circle mr-3">
                          <i className="fas fa-cog"></i>
                        </div>
                      </div>
                      <div>
                        <span className="h6 mb-0">Lorem ipsum dolor sit amet consectetur</span>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-lg-6">
              <img alt="Home placeholder" src={ smartPhones } className="img-fluid img-center hover-scale-110" />
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  )
}
