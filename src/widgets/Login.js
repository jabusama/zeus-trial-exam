import React, { useState } from 'react'

function Login({onSubmit}) {

  const initState = {
    username: '',
    password: ''
  }
  const [ user, setUser ] = useState(initState);

  const onSubmithandler = (e) => {
    e.preventDefault();
    onSubmit(user);
  }

  const onChangeHandler = (e) => {
    console.log(e.target.name)
    setUser({
      ...user,
      [e.target.name]: e.target.value
    })
  }

  return (
    <div>
      <form onSubmit={onSubmithandler}>
        <label htmlFor="username">Username</label>
        <input 
          type="text" 
          name="username"
          id="username"
          onChange={onChangeHandler} 
          value={user.useraname}
          data-testid="username"
          placeholder="Username"
        />
        <label htmlFor="password">Password</label>
        <input 
          type="password" 
          name="password" 
          id="password"
          onChange={onChangeHandler} 
          value={user.password}
          data-testid="password"
          placeholder="Password"
        />
        <button type="submit">Submit</button>
      </form>
    </div>
  )
}

export default Login;
