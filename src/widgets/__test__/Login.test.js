import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import Login from './../Login';

afterEach(cleanup);

test("renders without crashing", () => {
  const div = document.createElement("div");
  render(<Login></Login>, div);
})

it('renders correctly', () => {
  const { getByLabelText, getByText, getByPlaceholderText, getByTestId } = render(<Login></Login>)

  expect(getByTestId(/username/i)).toBeTruthy()
  expect(getByPlaceholderText(/password/i)).toBeTruthy()
  expect(getByText(/submit/i)).toBeTruthy()
})

test('calls onSubmit with username and password', () => {
  const handleSubmit = jest.fn();
  const {getByTestId, getByText} = render(<Login onSubmit={handleSubmit}></Login>)
  const username = getByTestId(/username/i)
  const password = getByTestId(/password/i)

  fireEvent.change(username, {target: {value: 'test'}})
  fireEvent.change(password, {target: {value: 'pass'}})
  fireEvent.click(getByText(/submit/i))
  
  expect(handleSubmit).toHaveBeenCalledTimes(1)
  expect(handleSubmit).toHaveBeenCalledWith({
    username: 'test',
    password: 'pass'
  })
})