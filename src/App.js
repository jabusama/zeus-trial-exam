import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import routeLinks from './routes'
import './App.css';

function App() {
  return (
    <Router>
      <div className="App" data-testid="App">
        <Switch>
          { routeLinks.map(route => <Route {...route} path={route.path} />)}
        </Switch>
      </div>
    </Router>
  );
}

export default App;
