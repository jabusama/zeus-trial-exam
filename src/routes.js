import Home from './views/home';
import { SignIn, SignUp } from './views/auth'

const routeLinks = [
  {
    key: 'home',
    path: '/',
    label: 'Home',
    exact: true,
    component: Home
  },
  {
    key: 'projects',
    path: '#projects',
    label: 'Projects'
  },
  {
    key: 'about-us',
    path: '#about-us',
    label: 'About Us'
  },
  {
    key: 'contact-us',
    path: '#contact-us',
    label: 'Contact Us'
  },
  {
    key: 'login',
    path: '/login',
    label: 'Login',
    class: 'ml-lg-5',
    component: SignIn
  },
  {
    key: 'register',
    path: '/register',
    label: 'Register',
    class: 'ml-lg-5',
    component: SignUp
  },
]

export default routeLinks;