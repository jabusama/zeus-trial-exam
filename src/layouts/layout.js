import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'

export default function layout({ children }) {
  return (
    <div>
      <Header />
      <div className="main-content">
        { children }
      </div>
      <Footer />
    </div>
  )
}
